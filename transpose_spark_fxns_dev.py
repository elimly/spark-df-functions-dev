
import pyspark.sql.functions as F
from pyspark.sql import Row
from pyspark.sql.types import *


def transpose_spark_df_row(row, spark_context, row_label="value", cast_type=None):
	row_list = [Row(key=k, value=str(v)) for k,v in row.asDict().items()]
	sdf = spark_context.parallelize(row_list).toDF(["key", row_label])
	
	if cast_type:
		sdf = sdf.withColumn(row_label, sdf[row_label].cast(cast_type))
	return sdf



def transpose_spark_df(df, spark_context, cast_type=None):
	# variables
	df_T_num_cols = df.count()
	df_T_cols = ['column_name'] + ['_row'+str(i) for i in range(0, df_T_num_cols)]
	
	dataType_dict = {'string':StringType(), 'int':IntegerType(), 'double':DoubleType(), 'float':FloatType(), 'long':LongType()}
	dataType = StringType() ## default: cast to string, unless otherwise specified
	
	## generate aggregate expressions:
	if cast_type is None: ## do not cast
		agg_expr = [F.collect_list(c).alias(c) for c in df.columns]
	else:
		if cast_type in dataType_dict: ## cast to specified Spark DataType
			dataType = dataType_dict[cast_type]
		agg_expr = [F.collect_list(c).cast(ArrayType(dataType)).alias(c) for c in df.columns]
	
	## 1) aggregate each column --> list
	df_list_sdf = df.groupBy().agg(*agg_expr)
	
	## 2) extract the 1st row values
	df_row = df_list_sdf.collect()[0]

	## 3) transpose sDF (create new sDF with row: col name + list of col values in original sDF)
	df_T_sdf = spark_context.parallelize([[c] + df_row[c] for c in df_list_sdf.columns])\
							.toDF(df_T_cols)
	return df_T_sdf



###############################################################################
## Deprecated:
###############################################################################

def transpose_spark_df_row_cast(row, spark_context, row_label="value", cast_type=None):
	row_list = [Row(key=k, value=str(v)) for k,v in row.asDict().items()]
	sdf = spark_context.parallelize(row_list).toDF(["key", row_label])

	if cast_type:
		sdf = sdf.withColumn(row_label, sdf[row_label].cast(cast_type))
	return sdf



def transpose_spark_df_noCast(df, spark_context):
	## variables
	agg_expr = [F.collect_list(c).alias(c) for c in df.columns]
	df_T_num_cols = df.count()
	df_T_cols = ['column_name'] + ['_row'+str(i) for i in range(0, df_T_num_cols)]

	## 1) aggregate each column --> list
	df_list_sdf = df.groupBy().agg(*agg_expr)

	## 2) extract the 1st row values
	df_row = df_list_sdf.collect()[0]

	## 3) transpose sDF (create new sDF with row: col name + list of col values in original sDF)
	df_T_sdf = spark_context.parallelize([[c] + df_row[c] for c in df_list_sdf.columns])\
							.toDF(df_T_cols)
	return df_T_sdf




def sdf_to_long(df):
	key_value_structs = F.explode(
		F.array([F.struct(F.lit(c).alias("key"), F.col(c).cast("string").alias("val")) for c in df.columns])).alias("kvs")
	return df.select(key_value_structs).select(["kvs.key", "kvs.val"])



