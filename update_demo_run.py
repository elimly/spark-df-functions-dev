
import os, subprocess, warnings, sys
warnings.filterwarnings('ignore')

from importlib import reload
from itertools import chain

from pyspark.sql import SparkSession
from pyspark.sql.types import *
import pyspark.sql.functions as F

## import custom Python module
import spark_df_functions as sdf_fxn ## Spark DF functions custom python module
reload(sdf_fxn)    ## remove this later


#### Spark setup
spark = SparkSession.builder.appName('spark-fxn-demo').getOrCreate()
sc = spark.sparkContext


######### Specify demo file paths:
#### input files:
sv_bed_file = "test_input_files/parliament_pass_filtered_dels_Chr1.bed"
overlap_file = "test_input_files/sort_overlap_demo.txt"
tissue_info_file = "test_input_files/tissue_info_table_gtex.csv"

#### output file paths:
output_dir = "test_output_spark-df-fxns"
shell_script_dir = "src"




## Helper function to display Spark DF in Pandas format
def print_sdf(sdf, nrow=5):
	print(sdf.limit(nrow).toPandas().head(nrow))
	return sdf.limit(nrow).toPandas().head(nrow)


#############################################################################
## load Spark DataFrames from demo files using 'spark_df_functions.py' module
#############################################################################

##### Example #1:
def load_example1():
	overlap_sdf = sdf_fxn.load_with_header(overlap_file, '\t', spark)
	
	print("# of rows = ", overlap_sdf.count())
	overlap_sdf.show(3)
	overlap_sdf.printSchema()
	print_sdf(overlap_sdf)
	return overlap_sdf


##### Example #2:  .csv file
def load_example2():
	tiss_sdf = sdf_fxn.load_with_header(tissue_info_file, ',', spark)
	
	print("# of rows = ", tiss_sdf.count())
	tiss_sdf.printSchema()
	print_sdf(tiss_sdf)
	return tiss_sdf


##### Example #3: bed like text file WITHOUT a header - using load_name_columns()
def load_example3():
	col_names = ['sv_chr', 'sv_start', 'sv_end', 'name', 'sv_type']
	sv_sdf = sdf_fxn.load_name_columns(sv_bed_file, '\t', col_names, spark)
	
	print("# of rows = ", sv_sdf.count())
	sv_sdf.printSchema()
	print_sdf(sv_sdf)
	return sv_sdf


#############################################################################
### Examples of how to add chrN column via map using new functions in spark_df_function.py:
#############################################################################
def example_add_chr_int_col(sdf):
	## 1) create mapping object:
	chr_mapping = sdf_fxn.generate_chr_mapping()
	print(chr_mapping)
	
	## 2) add new column with chr as int
	sdf2 = sdf_fxn.add_chr_int_column(sdf, 'sv_chr', 'chrN', chr_mapping)
	print_sdf(sdf2)
	
	return sdf2


#############################################################################
## Sorting by chr
#############################################################################

#### but first, here's a DEFAULT sorting example:
def example_default_sort(sdf):
	default_sort_sdf = sdf.orderBy('sv_chr', 'sv_start', 'sv_end')
	print_sdf(default_sort_sdf, 20)
	return default_sort_sdf


#### sort_sdf_by_chr() example:
def example_sort_by_chr(sdf):
	sorted_sdf = sdf_fxn.sort_sdf_by_chr(sdf, 'sv_chr', ['sv_start', 'sv_end'])
	print_sdf(sorted_sdf, 20)
	return sorted_sdf



if __name__ == "__main__":
	print("Started. Running in all-spark-notebook Docker container\n")
	overlap_sdf = load_example1()
	example_add_chr_int_col(overlap_sdf)
	
	

