#!/usr/bin/python -u
import warnings
warnings.filterwarnings('ignore')
from importlib import reload

from pyspark.sql import SparkSession
from pyspark.sql.types import *
import pyspark.sql.functions as F

import spark_df_functions as sdf_fxn ## Spark DF functions custom python module
reload(sdf_fxn)




#### SV level summary stats - annotation counts per SV
def generate_sv_annot_counts(ovrlp_sdf, sv_col, ft_col, ovrlp_col):
	ovrlp_sdf = ovrlp_sdf.dropDuplicates()\
					 .orderBy(sv_col)
	
	sv_summ_stat_sdf = ovrlp_sdf.groupBy(sv_col + ['sv_len'])\
					.agg(F.count('*').alias('Total_#_ft'),
						 F.countDistinct('ft_name').alias('#_distinct_ft_names'),
						 F.countDistinct(*ft_col).alias('#_distinct_ft_coord'),
						 F.countDistinct(*ovrlp_col).alias('#_distinct_ovrlp_coord'))\
					.orderBy(sv_col)\
					.cache()
	
	sdf_fxn.print_sdf(sv_summ_stat_sdf)
	return sv_summ_stat_sdf



#### annotation level summary stats - annot Totals & by annot feature name
def generate_annotation_summ_stats(full_annot_sdf, sv_cols, ft_cols, ovrlp_cols):
	full_annot_sdf = full_annot_sdf.dropDuplicates()
	
	## 1) calculate overall Totals
	annot_total_sdf = full_annot_sdf.groupBy()\
								.agg(F.countDistinct('ft_name').alias('#_distinct_ft_names'),
									 F.countDistinct(*(['ft_name'] + ft_cols)).alias('Total_#_ft'),
									 F.countDistinct(*ft_cols).alias('#_distinct_ft_coord'),
									 F.countDistinct(*ovrlp_cols).alias('#_distinct_ovrlp_coord'),
									 F.countDistinct(*sv_cols).alias('#_distinct_sv_coord'))
	annot_total_sdf.show(3)
	
	
	## 2) calc totals per feature name
	annot_count_sdf = full_annot_sdf.groupBy('ft_name')\
								.agg(F.countDistinct(*ft_cols).alias('#_distinct_ft_coord'),
									 F.countDistinct(*ovrlp_cols).alias('#_distinct_ovrlp_coord'),
									 F.countDistinct(*sv_cols).alias('#_distinct_sv_coord'))
	annot_count_sdf.show(3)
	
	
	## 3) calc averages per annot feature name
	ft_count_sdf = full_annot_sdf.groupBy(['ft_name'] + ft_cols)\
								.agg(F.countDistinct(*sv_cols).alias('#_sv_per_ft_coord'),
									 F.countDistinct(*ovrlp_cols).alias('#_ovrlp_coord_per_ft_coord'))\
								.orderBy('ft_name', ascending=False)
	
	ft_stat_sdf = ft_count_sdf.groupBy('ft_name')\
								.agg(F.avg('#_ovrlp_coord_per_ft_coord'),
									 F.avg('#_sv_per_ft_coord'))
	ft_stat_sdf.show(3)
	
	
	## 4) join ft name totals & averages Spark DFs
	annot_summ_stat_sdf = annot_count_sdf.join(ft_stat_sdf, on='ft_name', how='inner')\
										.orderBy('ft_name', ascending=False)
	annot_summ_stat_sdf.show(3)
	
	return annot_total_sdf, annot_summ_stat_sdf
		





#### subject level summary statistics
def subject_annotation_summ_stats(ovrlp_subj_sdf, sv_full_subj_sdf, sv_annot_count_sdf, subj_sv_stats_sdf, sv_cols, ft_cols, ovrlp_cols):
	ovrlp_len = ovrlp_cols[0].strip('chr') + 'len'
	reorder_cols = ['subj_id'] + sv_cols + ['sv_len', 'sv_type'] + sv_annot_count_sdf.columns[4:]
	
	## 1) subject - annotation TOTALs:
	subj_total_sdf = ovrlp_subj_sdf.groupBy('subj_id')\
						.agg(F.count('*').alias('Total_#_ft'),
							F.countDistinct('ft_name').alias('#_distinct_ft_names'),
							F.countDistinct(*ft_cols).alias('#_distinct_ft_coord'),
							F.countDistinct(*ovrlp_cols).alias('#_distinct_ovrlp_coord'),
							F.avg('ft_len').alias("ft_len.AVG"),
							F.min('ft_len').alias("ft_len.MIN"),
							F.max('ft_len').alias("ft_len.MAX"),
							F.stddev('ft_len').alias("ft_len.STDDEV"),
							F.avg(ovrlp_len).alias("overlap_len.AVG"),
							F.min(ovrlp_len).alias("overlap_len.MIN"),
							F.max(ovrlp_len).alias("overlap_len.MAX"),
							F.stddev(ovrlp_len).alias("overlap_len.STDDEV"),
							F.avg('proportion_of_ft').alias("proportion_of_ft.AVG"),
							F.min('proportion_of_ft').alias("proportion_of_ft.MIN"),
							F.max('proportion_of_ft').alias("proportion_of_ft.MAX"),
							F.stddev('proportion_of_ft').alias("proportion_of_ft.STDDEV"),
							F.avg('proportion_of_sv').alias("proportion_of_sv.AVG"),
							F.min('proportion_of_sv').alias("proportion_of_sv.MIN"),
							F.max('proportion_of_sv').alias("proportion_of_sv.MAX"),
							F.stddev('proportion_of_sv').alias("proportion_of_sv.STDDEV"))\
						.cache()
	
	## 2) join full_subj_sv_sdf & sv_annot_count_sdf --> add subj IDs to each SV
	subj_sv_annot_count_sdf = sv_full_subj_sdf.join(sv_annot_count_sdf, on=sv_cols + ['sv_len'], how="inner")\
											.select(reorder_cols)\
											.repartition('subj_id')
	
	## 3) subject - annotations per SV:
	subj_sv_sdf = subj_sv_annot_count_sdf.groupBy(['subj_id'])\
						.agg(F.avg('Total_#_ft').alias('perSV.Total_#_ft.AVG'),
							 F.min('Total_#_ft').alias('perSV.Total_#_ft.MIN'),
							 F.max('Total_#_ft').alias('perSV.Total_#_ft.MAX'),
							 F.stddev('Total_#_ft').alias('perSV.Total_#_ft.STDDEV'),
							 F.avg('#_distinct_ft_names').alias('perSV.#_distinct_ft.AVG'),
							 F.min('#_distinct_ft_names').alias('perSV.#_distinct_ft.MIN'),
							 F.max('#_distinct_ft_names').alias('perSV.#_distinct_ft.MAX'),
							 F.stddev('#_distinct_ft_names').alias('perSV.#_distinct_ft.STDDEV'),
							 F.avg('#_distinct_ft_coord').alias('perSV.#_ft_coord.AVG'),
							 F.min('#_distinct_ft_coord').alias('perSV.#_ft_coord.MIN'),
							 F.max('#_distinct_ft_coord').alias('perSV.#_ft_coord.MAX'),
							 F.stddev('#_distinct_ft_coord').alias('perSV.#_ft_coord.STDDEV'),
							 F.avg('#_distinct_ovrlp_coord').alias('perSV.#_ovrlp_coord.AVG'),
							 F.min('#_distinct_ovrlp_coord').alias('perSV.#_ovrlp_coord.MIN'),
							 F.max('#_distinct_ovrlp_coord').alias('perSV.#_ovrlp_coord.MAX'),
							 F.stddev('#_distinct_ovrlp_coord').alias('perSV.#_ovrlp_coord.STDDEV'))\
						.cache()

	#### 3) join with Subject SV total sDF:
	subj_stat_sdf = subj_sv_stats_sdf.join(subj_total_sdf, on='subj_id', how="inner")\
									.join(subj_sv_sdf, on='subj_id', how="inner")\
									.orderBy('subj_id')\
									.cache()

	return subj_stat_sdf

