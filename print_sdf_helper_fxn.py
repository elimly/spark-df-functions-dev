"""
Helper functions to nicely display Spark DataFrames
	(Spark DF in Pandas format)
"""

## use this for Jupyter Notebooks:
def print_sdf(sdf, nrow=5):
	return sdf.limit(nrow).toPandas().head(nrow)

## use this in Python programs:
def print_sdf(sdf, nrow=5):
	print(sdf.limit(nrow).toPandas().head(nrow))
	return sdf.limit(nrow).toPandas().head(nrow)

