#!/usr/bin/env bash 
TMPDIR=$1
OUTDIR=$2
FILENAME=$3
HEADERSTR=$4



## step 1: sort spark output file
if [ "$(uname)" == "Darwin" ]; then
	gsort -k1,1 -k2,2 -k3,3 -V -s ${TMPDIR}/part* -o ${TMPDIR}/${FILENAME}

	gsed -i.old $'1i '${HEADERSTR} ${TMPDIR}/${FILENAME}

else
	sort -k1,1 -k2,2 -k3,3 -V -s ${TMPDIR}/part* -o ${TMPDIR}/${FILENAME}

	sed -i.old $'1i '${HEADERSTR} ${TMPDIR}/${FILENAME}
fi




## step 2: move new sorted output file & delete tmp spark outout directory
mv ${TMPDIR}/${FILENAME} ${OUTDIR}/${FILENAME}
rm -r ${TMPDIR}


