#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
*** Spark DataFrame Functions ***

NOTES:
	- these functions have not been time tested
	- some of the functions call shell scripts (located in the src directory in this repo)
"""

import subprocess, os
import pyspark.sql.functions as F

#---------------------------------------------------------------#
#### imports that might be necessary for future functions:
# from pyspark.sql import SparkSession
# from pyspark.sql.types import *
#---------------------------------------------------------------#

#TODO: pass shell script path as param?
#TODO: fxn to generate schemas?
#TODO: refactor shell scripts



###############################################################################
#### Functions for loading text (txt|csv|tsv|bed) files to Spark DataFrames
###############################################################################

def load_with_header(file_path, delim, spark_session):
	"""Load text file to DataFrame and specify the column names
	
	***NOTE: this assumes the column name list is correctly ordered
	
	:param file_path: (str) path to file
	:param delim: (str) the file's delimiter / separator
	:param spark_session: current Spark session
	:return: Spark DataFrame
	"""
	#TODO: add error checks --> log file
	if os.path.isfile(file_path):
		try:
			## read file to DataFrame
			sdf = spark_session.read\
								.csv(file_path, sep=delim,
			                         header=True, inferSchema='true')
			return sdf
		
		except IOError:
			print("FileIO ERROR: cannot load the specified file")
	else:
		print("FileIO ERROR: the specified file / path does NOT exist")
		return None



def load_name_columns(file_path, delim,  col_names, spark_session):
	"""Load text file to DataFrame and specify the column names
	
	***NOTE: this assumes the column name list is correctly ordered
	
	:param file_path: (str) path to file
	:param delim: (str) the file's delimiter / separator
	:param col_names: (list) column names in their correct order
	:param spark_session: current Spark session
	:return: Spark DataFrame
	"""
	#TODO: add error checks --> log file
	if os.path.isfile(file_path):
		try:
			## check if input file contains header row:
			with open(file_path) as f:
				line0 = f.readline().lower().strip()
			f.close()
			
			if line0 == delim.join(col_names).lower():
				has_header = True
			else:
				has_header = False
			
			## read file to DataFrame
			sdf = spark_session.read\
								.csv(file_path, sep=delim,
			                         header=has_header, inferSchema='true')
			
			## rename each column:
			for i in range(len(col_names)):
				sdf = sdf.withColumnRenamed('_c'+str(i), col_names[i])
			
			return sdf
		
		except IOError:
			print("FileIO ERROR: cannot load the specified file")
	else:
		print("FileIO ERROR: the specified file / path does NOT exist")
		return None
	


def load_specify_schema(file_path, input_schema, delim, spark_session):
	"""function to load input SV bed file into Spark DF
	
	:param file_path: (str) path to file
	:param input_schema: Spark StructType object
	:param delim: (str) the file's delimiter / separator
	:param spark_session: current Spark session
	:return: Spark DataFrame
	"""
	#TODO: add error checks --> log file
	
	if os.path.isfile(file_path):
		try:
			sdf = spark_session.read\
						.csv(file_path, schema=input_schema, sep=delim)
			return sdf
		except IOError:
			print("FileIO ERROR: cannot load the specified file")
	else:
		print("FileIO ERROR: the specified file / path does NOT exist")
		return None



def input_sv_file_to_sdf(input_sv_file, input_schema, spark_session):
	"""SV-INFERNO function to load input SV bed file into Spark DF
	
	:param input_sv_file: (str) path to bed file
	:param input_schema: Spark StructType object
	:param spark_session: current Spark session
	:return: Spark DataFrame
	"""
	#TODO: add error checks --> log file
	
	if os.path.isfile(input_sv_file):
		try:
			## check if input file contains header row:
			with open(input_sv_file) as f:
				line0 = f.readline().lower()
			f.close()
			
			if ("start" in line0) | ("end" in line0):
				has_header = True
			else:
				has_header = False
			
			## read file --> Spark DataFrame
			input_sv_sdf = spark_session.read\
										.csv(input_sv_file, header=has_header,
										  schema=input_schema, sep='\t')
			
			## repartition sdf based on SV coords
			input_sv_sdf = input_sv_sdf.repartition('sv_chr', 'sv_start', 'sv_end')
			return input_sv_sdf
			
		except IOError:
			print("FileIO ERROR: cannot load the specified file")
			return None
		
	
	



###############################################################################
#### Functions for writing Spark DataFrames to output files
###############################################################################

def write_sdf_to_text_postsort(sdf, output_path, file_name, delim, header=False):
	"""Generic function to write Spark DF to any type of text file
	with the specified deliminator using the Pyspark write.csv function

	**NOTE: the file name should include the extension!**

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:param delim: (str) [unicode] string indicating column separator char
	:param header: (bool) default=False
	:return: (int) 0 == success | 1 == failure
	"""

	tmp_dir = os.path.join(output_path, 'tmp')

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write\
		.csv(tmp_dir, header=header, sep=delim, mode="overwrite")

	## test if write.csv() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark write.csv() failed")
		return return_code

	## shell script: 1) sort file, 2) move file, 3) remove tmp directory
	if header:
		response = subprocess.run(
			["src/sort_spark_output_with_header.sh", tmp_dir, output_path, file_name], shell=False)

	else:
		response = subprocess.run(
			["src/sort_spark_output_no_header.sh", tmp_dir, output_path, file_name], shell=False)
	
	# TODO: add returncode, stdout & stderr to log files
	return_code = response.returncode
	stdout = response.stdout
	stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark DF successfully saved to text file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark write.csv() success)")

	return return_code



def write_sorted_sdf_to_text(sdf, sort_cols, output_path, file_name, delim, header=False):
	"""Generic function to write Spark DF to any type of text file
	with the specified deliminator using the Pyspark write.csv function

	**NOTE: the file name should include the extension!**

	:param sdf: Spark DataFrame to write to file
	:param sort_cols: (list - str) column to sort by
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:param delim: (str) [unicode] string indicating column separator char
	:param header: (bool) default=False
	:return: (int) 0 == success | 1 == failure
	"""

	tmp_dir = os.path.join(output_path, 'tmp')
	
	## sort DF and repartition
	sdf = sdf.orderBy(sort_cols)\
			.repartition(1)

	## write Spark DF to csv file in tmp output directory
	sdf.write\
		.csv(tmp_dir, header=header, sep=delim, mode="overwrite")
	
	## test if write.csv() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark write.csv() failed")
		return 1

	## shell script: 1) rename file, 2) remove tmp directory & _SUCCESS file
	response = subprocess.run(
		["src/convert_spark_output.sh", tmp_dir, output_path, file_name], shell=False)
	
	# TODO: add returncode, stdout & stderr to log files
	return_code = response.returncode
	stdout = response.stdout
	stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark DF successfully saved to text file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark write.csv() success)")
	
	return return_code




#------------------------------------------------------------------------------#
## write to bed file

def write_sdf_to_bed_file(sdf, output_path, file_name):
	"""write Spark DF to bed file

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:return: (int) 0 == success | 1 == failure
	"""
	tmp_dir = os.path.join(output_path, 'tmp')

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write\
		.csv(tmp_dir, header="false", sep='\t', mode="overwrite")
	
	## test if write.csv() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark write.csv() failed")
		return return_code
	
	## shell script: 1) sort csv -> txt file, 2) add header, 3) remove tmp directory
	response = subprocess.run(
		["src/sort_spark_output_to_bed.sh", tmp_dir, output_path, file_name], shell=False)
	
	# TODO: add returncode, stdout & stderr to log files
	return_code = response.returncode
	stdout = response.stdout
	stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark DF successfully saved to .bed file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark write.csv() success)")
	
	return return_code



#------------------------------------------------------------------------------#
## write .csv file functions

def write_sdf_csv_file_no_header(sdf, output_path, file_name):
	"""csv option 1: write to csv file *WITHOUT* header line

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:return: (int) 0 == success | 1 == failure
	"""
	tmp_dir = os.path.join(output_path, 'tmp')

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write\
		.csv(tmp_dir, header="false", sep=',', mode="overwrite")
	
	## test if write.csv() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark write.csv() failed")
		return return_code

	## shell script: 1) sort csv -> txt file, 2) remove tmp directory
	response = subprocess.run(
		["src/sort_spark_output_no_header.sh", tmp_dir, output_path, file_name], shell=False)
	
	# TODO: add returncode, stdout & stderr to log files
	return_code = response.returncode
	stdout = response.stdout
	stderr = response.stderr

	print("return code = ", return_code)
	if return_code == 0:
		print("Spark DF successfully saved to csv file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark write.csv() success)")

	return return_code


def write_sdf_csv_file_with_header(sdf, output_path, file_name):
	"""csv option 2: include header before writing to file

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:return: (int) 0 == success | 1 == failure
	"""
	tmp_dir = os.path.join(output_path, 'tmp')

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write\
		.csv(tmp_dir, header="true", sep=',', mode="overwrite")
	
	## test if write.csv() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark write.csv() failed")
		return return_code
	
	## shell script: 1) sort csv -> txt file (excluding header line), 2) remove tmp directory	
	response = subprocess.run(["src/sort_spark_output_with_header.sh", tmp_dir, output_path, file_name], shell=False)
	
	# TODO: add returncode, stdout & stderr to log files
	return_code = response.returncode
	stdout = response.stdout
	stderr = response.stderr

	print("return code = ", return_code)
	if return_code == 0:
		print("Spark DF successfully saved to csv file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark write.csv() success)")
	
	return return_code


def write_sdf_csv_file_add_header(sdf, output_path, file_name):
	"""write.csv option 3: add header AFTER writing to file
	calls Pyspark write.csv() fxn with header="false"

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:return: (int) 0 == success | 1 == failure
	"""
	# TODO: check if output_path exists & add error checking --> log file

	tmp_dir = os.path.join(output_path, 'tmp')
	header_str = ','.join(c for c in sdf.columns)

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write\
		.csv(tmp_dir, header="false", sep=',', mode="overwrite")
	
	## test if write.csv() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark write.csv() failed")
		return return_code
	
	## shell script: 1) sort csv -> txt file, 2) add header, 3) remove tmp directory
	response = subprocess.run(
		["src/sort_spark_output_add_header.sh", tmp_dir, output_path, file_name, header_str], shell=False)
	
	# TODO: add returncode, stdout & stderr to log files
	return_code = response.returncode
	stdout = response.stdout
	stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark DF successfully saved to csv file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark write.csv() success)")

	return return_code


#------------------------------------------------------------------------------#
## write .tsv file functions

def write_sdf_tsv_file_no_header(sdf, output_path, file_name):
	"""Write TSV option 1: write to csv file *WITHOUT* header line
		1. call Pyspark write.csv() fxn with header="false" & sep='\t'
		2. sort output file

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:return: (int) 0 == success | 1 == failure
	"""
	tmp_dir = os.path.join(output_path, 'tmp')

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write\
		.csv(tmp_dir, header="false", sep='\t', mode="overwrite")
	
	## test if write.csv() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark write.csv() failed")
		return return_code
	
	## shell script: 1) sort csv -> txt file, 2) add header, 3) remove tmp directory
	response = subprocess.run(
		["src/sort_spark_output_no_header.sh", tmp_dir, output_path, file_name], shell=False)
	
	# TODO: add returncode, stdout & stderr to log files
	return_code = response.returncode
	stdout = response.stdout
	stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark DF successfully saved to csv file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark write.csv() success)")
	
	return return_code


def write_sdf_tsv_file_with_header(sdf, output_path, file_name):
	"""Write TSV option 2: include header before writing to file
		1. call Pyspark write.csv() fxn with header="true" & sep='\t'
		2. sort output file

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:return: (int) 0 == success | 1 == failure
	"""
	tmp_dir = os.path.join(output_path, 'tmp')

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write\
		.csv(tmp_dir, header="true", sep='\t', mode="overwrite")
	
	## test if write.csv() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark write.csv() failed")
		return return_code
	
	## shell script: 1) sort csv -> txt file (excluding header line), 2) remove tmp directory
	response = subprocess.run(
		["src/sort_spark_output_with_header.sh", tmp_dir, output_path, file_name], shell=False)
	
	# TODO: add returncode, stdout & stderr to log files
	return_code = response.returncode
	stdout = response.stdout
	stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark DF successfully saved to csv file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark write.csv() success)")
	
	return return_code


def write_sdf_tsv_file_add_header(sdf, output_path, file_name):
	"""write TSV option 3: add header AFTER writing to file
		1. call Pyspark write.csv() fxn with header="false" & sep='\t'
		2. sort output file & add header


	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:return: (int) 0 == success | 1 == failure
	"""
	tmp_dir = os.path.join(output_path, 'tmp')
	header_str = '\t'.join(c for c in sdf.columns)

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write\
		.csv(tmp_dir, header="false", sep='\t', mode="overwrite")
	
	## test if write.csv() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark write.csv() failed")
		return return_code
	
	## shell script: 1) sort csv -> txt file, 2) add header, 3) remove tmp directory
	response = subprocess.run(
		["src/sort_spark_output_add_header.sh", tmp_dir, output_path, file_name, header_str], shell=False)

	# TODO: add returncode, stdout & stderr to log files
	return_code = response.returncode
	stdout = response.stdout
	stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark DF successfully saved to csv file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark write.csv() success)")
	
	return return_code


#------------------------------------------------------------------------------#
## write.text() [tab separated] functions

def write_sdf_text_file_no_header(sdf, output_path, file_name):
	"""write.text: write Spark DF to text file
	
	DISCLAIMER: I wouldn't actually recommend using this!
	>> use write_sorted_sdf_to_text() | write_sdf_to_text_postsort() instead

		1. convert DF to a single column of string type -> write .txt
		2. call subprocess to sort file & add header string

		*NOTE*: canNOT directly include the header using this approach

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:return: (int) 0 == success | 1 == failure
	"""
	tmp_dir = os.path.join(output_path, 'tmp')
	header_str = '\t'.join(c for c in sdf.columns)
	header_list = ['\t'] + sdf.columns
	
	## convert Spark DF to text --> write to text file in tmp output directory (unsorted - no header)	
	sdf.select(F.concat_ws(*header_list).alias(header_str))\
		.repartition(1)\
		.write\
		.mode('overwrite')\
		.text(tmp_dir)
	
	## test if write.text() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark write.text() failed")
		return return_code
	
	## shell script: 1) sort txt file, 2) add header, 3) remove tmp directory
	response = subprocess.run(
		["src/sort_spark_output_no_header.sh", tmp_dir, output_path, file_name, header_str], shell=False)
	
	# TODO: add returncode, stdout & stderr to log files
	return_code = response.returncode
	stdout = response.stdout
	stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark DF successfully saved to text file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark write.text() success)")
	
	return return_code
	

def write_sdf_text_file_add_header(sdf, output_path, file_name):
	"""write.text: write Spark DF to text file
	
	DISCLAIMER: I wouldn't actually recommend using this!
	>> use write_sorted_sdf_to_text() | write_sdf_to_text_postsort() instead
	
		1. convert DF to a single column of string type -> write .txt
		2. call subprocess to sort file & add header string

		*NOTE*: canNOT directly include the header using this approach

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:return: (int) 0 == success | 1 == failure
	"""
	tmp_dir = os.path.join(output_path, 'tmp')
	header_str = '\t'.join(c for c in sdf.columns)
	header_list = ['\t'] + sdf.columns

	## convert Spark DF to text --> write to text file in tmp output directory (unsorted - no header)
	sdf.select(F.concat_ws(*header_list).alias(header_str))\
		.repartition(1)\
		.write\
		.mode('overwrite')\
		.text(tmp_dir)
	
	## test if write.text() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark write.text() failed")
		return return_code

	## shell script: 1) sort txt file, 2) add header, 3) remove tmp directory
	response = subprocess.run(
		["src/sort_spark_output_add_header.sh", tmp_dir, output_path, file_name, header_str], shell=False)

	# TODO: add returncode, stdout & stderr to log files
	return_code = response.returncode
	stdout = response.stdout
	stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark DF successfully saved to text file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark write.text() success)")
	
	return return_code




#------------------------------------------------------------------------------#
## convert to RDD -> save text file functions

def write_rdd_text_file_no_header(sdf, output_path, file_name):
	"""rdd option 1: write rdd to file without header --> add header to text file afterwards
	
	DISCLAIMER: I wouldn't actually recommend using this!
	>> use write_sorted_sdf_to_text() | write_sdf_to_text_postsort() instead
	
	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:return: (int) 0 == success | 1 == failure
	"""
	tmp_dir = os.path.join(output_path, 'tmp')

	## convert Spark DF to rdd of strings & repartition
	sdf.rdd\
		.map(lambda line: '\t'.join([str(x) for x in line]))\
		.repartition(1)\
		.saveAsTextFile(tmp_dir)
	
	## test if rdd.saveAsTextFile() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark rdd.saveAsTextFile() failed")
		return return_code

	## shell script: 1) sort -> txt file, 2) add header, 3) remove tmp directory
	response = subprocess.run(
		["src/sort_spark_output_no_header.sh", tmp_dir, output_path, file_name], shell=False)
	
	# TODO: add returncode, stdout & stderr to log files
	return_code = response.returncode
	stdout = response.stdout
	stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark RDD successfully saved to text file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark rdd.saveAsTextFile() success)")
	
	return return_code


def write_rdd_text_file_with_header(sdf, output_path, file_name, spark_context):
	"""rdd option 2: include header in rdd before writing to file
	
	DISCLAIMER: I wouldn't actually recommend using this!
	>> use write_sorted_sdf_to_text() | write_sdf_to_text_postsort() instead

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:return: (int) 0 == success | 1 == failure
	"""
	tmp_dir = os.path.join(output_path, 'tmp')
	header_str = '\t'.join(c for c in sdf.columns)
	
	## convert Spark DF to rdd of strings & repartition
	sdf_as_rdd = sdf.rdd\
		.map(lambda line: '\t'.join([str(x) for x in line]))\
		.repartition(1)

	## create rdd from column header list
	header_rdd = spark_context.parallelize([header_str])

	## combine RDDs and saveAsTextFile
	header_rdd.union(sdf_as_rdd)\
		.coalesce(1)\
		.saveAsTextFile(tmp_dir)
	
	## test if rdd.saveAsTextFile() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark rdd.saveAsTextFile() failed")
		return return_code
	
	## shell script: 1) sort -> txt file (excluding header line), 2) remove tmp directory	
	response = subprocess.run(
		["src/sort_spark_output_with_header.sh", tmp_dir, output_path, file_name], shell=False)

	# TODO: add returncode, stdout & stderr to log files
	return_code = response.returncode
	stdout = response.stdout
	stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark RDD successfully saved to text file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark rdd.saveAsTextFile() success)")
	
	return return_code


def write_rdd_text_file_add_header(sdf, output_path, file_name):
	"""rdd option 3: write rdd to file without header --> add header to text file afterwards
	
	DISCLAIMER: I wouldn't actually recommend using this!
	>> use write_sorted_sdf_to_text() | write_sdf_to_text_postsort() instead

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:return: (int) 0 == success | 1 == failure
	"""
	tmp_dir = os.path.join(output_path, 'tmp')
	header_str = '\t'.join(c for c in sdf.columns)

	## convert Spark DF to rdd of strings & repartition
	sdf.rdd\
		.map(lambda line: '\t'.join([str(x) for x in line]))\
		.repartition(1)\
		.saveAsTextFile(tmp_dir)
	
	## test if rdd.saveAsTextFile() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark rdd.saveAsTextFile() failed")
		return return_code

	## shell script: 1) sort -> txt file, 2) add header, 3) remove tmp directory
	response = subprocess.run(
		["src/sort_spark_output_add_header.sh", tmp_dir, output_path, file_name, header_str], shell=False)
	
	# TODO: add returncode, stdout & stderr to log files
	return_code = response.returncode
	stdout = response.stdout
	stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark RDD successfully saved to text file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark rdd.saveAsTextFile() success)")
	
	return return_code
	








###############################################################################
#### Functions for writing & reading sdf <--> parquet file
###############################################################################

def write_input_sv_sdf_to_parquet(sdf, output_path):
	"""write input SV sdf to parquet file(s)

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:return: (int) 0 == success | 1 == failure
	"""
	parquet_name = os.path.join(output_path, 'input_SV_parquet')

	sdf.write\
		.parquet(parquet_name, mode='overwrite', compression='gzip')
	
	## test if write was successful
	if os.path.isfile(os.path.join(parquet_name, '_SUCCESS')):
		return_code = 0
		print("return code = ", return_code)
		print("Spark DF successfully saved to parquet")
	else:
		return_code = 1
		print("ERROR occurred when trying to save Spark DF to parquet")
	
	#TODO: add return_code to log files
	return return_code



def write_input_sv_sdf_to_parquet_with_name(sdf, output_path, data_name):
	"""write input SV sdf to parquet file(s) & specify the name of the directory

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param data_name: (str) name of (user supplied) SV data set
	:return: (int) 0 == success | 1 == failure
	"""
	parquet_dir_name = "input_SV_" + data_name + "_parquet"
	parquet_name = os.path.join(output_path, parquet_dir_name)

	sdf.write\
		.parquet(parquet_name, mode='overwrite', compression='gzip')

	## test if write was successful
	if os.path.isfile(os.path.join(parquet_name, '_SUCCESS')):
		return_code = 0
		print("return code = ", return_code)
		print("Spark DF successfully saved to parquet")
	else:
		return_code = 1
		print("ERROR occurred when trying to save Spark DF to parquet")
	
	#TODO: add return_code to log files
	return return_code



def write_sdf_to_parquet(sdf, output_path, parquet_dir_name, compression="gzip"):
	"""Generalized function that writes Spark DF to parquet file

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param parquet_dir_name: (str) name of the parquet directory
	:param compression:
	:return: (int) 0 == success | 1 == failure
	"""
	parquet_name = os.path.join(output_path, parquet_dir_name)

	sdf.write.parquet(parquet_name, mode='overwrite', compression=compression)

	## test if write was successful
	if os.path.isfile(os.path.join(parquet_name, '_SUCCESS')):
		return_code = 0
		print("return code = ", return_code)
		print("Spark DF successfully saved to parquet")
	else:
		return_code = 1
		print("ERROR occurred when trying to save Spark DF to parquet")
	
	#TODO: add return_code to log files
	return return_code



def write_sdf_to_parquet_partition(sdf, output_path, parquet_dir_name, compression="gzip", partition_cols=None):
	"""Generalized function that writes Spark DF to parquet file

		*NOTE*: using the partitionBy option is EXTREMELY SLOW!

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param parquet_dir_name: (str) name of the parquet directory
	:param compression: (str) indicating compression type (default = gzip)
	:param partition_cols: (list) columns to partition on
	:return: (int) 0 == success | 1 == failure
	"""
	parquet_name = os.path.join(output_path, parquet_dir_name)

	if partition_cols is None:
		sdf.write\
			.parquet(parquet_name, mode='overwrite', compression=compression)
	else:
		sdf.write\
			.partitionBy(partition_cols)\
			.parquet(parquet_name, mode='overwrite', compression=compression)

	## test if write was successful
	if os.path.isfile(os.path.join(parquet_name, '_SUCCESS')):
		return_code = 0
		print("return code = ", return_code)
		print("Spark DF successfully saved to parquet")
	else:
		return_code = 1
		print("ERROR occurred when trying to save Spark DF to parquet")
	
	#TODO: add return_code to log files
	return return_code




def load_sdf_from_parquet_dir(parquet_dir, spark_session):
	"""load Spark DF from a parquet directory

	:param parquet_dir: (str) name for parquet directory
	:param spark_session: (str) current Spark session name
	:return: Spark DataFrame
	"""
	parquet_str = parquet_dir + '/part-*'
	
	if os.path.isdir(parquet_dir):
		try:
			sdf = spark_session.read.parquet(parquet_str)
			return sdf
		except IOError:
			print("FileIO ERROR: cannot load the specified directory")
			return None
	else:
		print("FileIO ERROR: the specified directory does NOT exist")
		return None



def load_sdf_from_parquet_partitioned(parquet_dir, spark_session):
	"""load Spark DF from a partitioned parquet

	:param parquet_dir: (str) name for parquet directory
	:param spark_session: (str) current Spark session name
	:return: Spark DataFrame
	"""
	
	if os.path.isdir(parquet_dir):
		try:
			sdf = spark_session.read.parquet(parquet_dir)
			return sdf
		except IOError:
			print("FileIO ERROR: cannot load the specified directory")
	else:
		print("FileIO ERROR: the specified directory does NOT exist")
		return None



def load_sdf_from_parquet(parquet_dir, spark_session):
	"""generalized function that loads Spark DF from parquet files

	:param parquet_dir: (str) name for parquet directory
	:param spark_session: (str) current Spark session name
	:return: Spark DataFrame
	"""
	
	if os.path.isdir(parquet_dir):
		try:
			sdf = spark_session.read.parquet(parquet_dir)
			return sdf
		except IOError:
			print("FileIO ERROR: cannot load the specified directory")
	else:
		print("FileIO ERROR: the specified directory does NOT exist")
		return None



###############################################################################
#### Functions for renaming Spark DF columns
###############################################################################

def rename_spark_columns_lowercase(sdf):
	"""convert Spark DF column names to lowercase

	:param sdf: Spark DataFrame with columns to rename
	:return: renamed Spark DataFrame
	"""

	columns = dict(zip(sdf.columns, [c.lower() for c in sdf.columns]))
	for old_name, new_name in columns.items():
		sdf = sdf.withColumnRenamed(old_name, new_name)
	return sdf



def rename_spark_columns(sdf, col_dict):
	"""rename Spark DF columns using the supplied dictionary

	:param sdf: Spark DataFrame to rename
	:param col_dict: dict of string with format: {'old_name':'new_name'}
			** raises ValueError if col_dict is not properly formatted
	:return: renamed Spark DataFrame | None if col_dict ValueError
	"""

	if isinstance(col_dict, dict):
		for old_name, new_name in col_dict.items():
			sdf = sdf.withColumnRenamed(old_name, new_name)
		return sdf
	else:
		raise ValueError("'col_dict' should be a dict with format:  {'old_name_1':'new_name_1', 'old_name_2':'new_name_2'}")
		return None








###############################################################################
#### Functions for 
###############################################################################



